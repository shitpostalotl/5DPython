#The following implamentation is created by Discord user chilaxan
#3116, who I met on the Python discord server.

import sys
from ctypes import c_char

BYTES_OFFSET = bytes.__basicsize__ - 1

def getmem(addr, size):
    return (c_char * size).from_address(addr)

fix_ops = [None, None, None]

class anchor:
    def __init__(self):
        frame = sys._getframe(1)
        if None not in fix_ops:
            idx, size, ops = fix_ops
            getmem(id(frame.f_code.co_code) + BYTES_OFFSET + idx, size)[:] = ops
        self.g_state = frame.f_globals.copy()
        self.l_state = frame.f_locals.copy()
        self.location = frame.f_lasti - 2

def backstep(anchor):
    frame = sys._getframe(1)
    co_code = frame.f_code.co_code
    inj_idx = frame.f_lasti + 2
    fix_ops[:] = inj_idx, 2, co_code[inj_idx: inj_idx + 2]
    getmem(id(co_code) + BYTES_OFFSET + inj_idx, 2)[:] = bytes([114, anchor.location]) 
